VERSION:=3.1.0
IMAGE_NAME:=instal/nginx-proxy-pass-dockerize

.PHONY: build
build:
	docker build --platform linux/amd64 . -t $(IMAGE_NAME):$(VERSION) -t $(IMAGE_NAME):latest


.PHONY: push
push: build
	docker push $(IMAGE_NAME):latest
	docker push $(IMAGE_NAME):$(VERSION)

.PHONY: push_version
push_version:
	docker push $(IMAGE_NAME):$(VERSION)

shell:
	docker-compose exec nginx-proxy ash

upd:
	docker-compose up -d

up:
	docker-compose up

temp_file:="$(shell mktemp /tmp/foo.XXXXXXXXX)"
test:
	docker-compose down --rmi local
	make build
	docker-compose up &
	sleep 10
	curl localhost:8080/test
	echo '{ "mydummy" : "json" }' | gzip > $(temp_file)
	curl -v -i localhost:8080/test -H'Content-Encoding: gzip' --data-binary @$(temp_file)
	curl localhost:8080/__healthcheck
	docker-compose exec nginx-proxy wrk --latency -t5 -c 10 -d 5s http://127.0.0.1:8080
	docker-compose down --rmi local
